﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace Ugolki
{
    public class Gameboard
    {
        /// <summary>
        /// Клетки доски - основное хранилище информации о состоянии на поле
        /// </summary>
        readonly Cell[,] board;
        /// <summary>
        /// Словарь для быстрого доступа по id
        /// </summary>
        Dictionary<int, Cell> cellsById;
        /// <summary>
        /// Вспомогательный словарь для быстрого поиска клетки по заданной фигуре
        /// </summary>
        Dictionary<int, Cell> cellsByFigureId;
        /// <summary>
        /// Фигуры на поле
        /// </summary>
        Dictionary<int, Figure> figures;
        /// <summary>
        /// Стартовые ячейки для фигур на доске
        /// </summary>
        Dictionary<FigureColor, List<Cell>> StartCells;

        public Cell this[int x, int y]
        {
            get { return board[x, y]; }
        }

        public int FiguresCount { get { return figures.Count; } }
        public int Size { get; private set; }

        public Gameboard(int boardSize, int startFieldSize = 1)
        {
            if (boardSize <= 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(boardSize)}");
            }
            if (startFieldSize <= 0 || startFieldSize > boardSize / 2)
            {
                throw new ArgumentOutOfRangeException($"{nameof(startFieldSize)}");
            }

            Size = boardSize;

            board = new Cell[boardSize, boardSize];
            cellsById = new Dictionary<int, Cell>();
            cellsByFigureId = new Dictionary<int, Cell>();

            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    var id = j * boardSize + i;
                    var cell = new Cell(id, i, j, (i + j) % 2 == 0 ? CellColor.Light : CellColor.Dark);
                    board[i, j] = cell;
                    cellsById[id] = cell;
                }
            }

            figures = new Dictionary<int, Figure>();
            int figureId = 1;

            StartCells = new Dictionary<FigureColor, List<Cell>>();
            StartCells[FigureColor.Light] = new List<Cell>();
            StartCells[FigureColor.Dark] = new List<Cell>();

            for (int i = 0; i < startFieldSize; i++)
            {
                for (int j = 0; j < startFieldSize; j++)
                {
                    var figure = new Figure(figureId, FigureColor.Light);
                    board[i, j].CurrentFigure = figure;
                    figures[figureId] = figure;
                    cellsByFigureId[figureId] = board[i, j];
                    StartCells[FigureColor.Light].Add(board[i, j]);
                    figureId++;
                }
            }

            for (int i = boardSize - startFieldSize; i < boardSize; i++)
            {
                for (int j = boardSize - startFieldSize; j < boardSize; j++)
                {
                    var figure = new Figure(figureId, FigureColor.Dark);
                    board[i, j].CurrentFigure = figure;
                    figures[figureId] = figure;
                    cellsByFigureId[figureId] = board[i, j];
                    StartCells[FigureColor.Dark].Add(board[i, j]);
                    figureId++;
                }
            }
        }

        public Figure GetFigure(int id)
        {
            if (id <= 0 || !figures.ContainsKey(id))
            {
                throw new ArgumentOutOfRangeException();
            }
            return figures[id];
        }

        public Cell GetCellByFigureId(int id)
        {
            if (id <= 0 || !cellsByFigureId.ContainsKey(id))
            {
                throw new ArgumentOutOfRangeException();
            }
            return cellsByFigureId[id];
        }

        public Cell GetCellById(int id)
        {
            if (id <= 0 || !cellsById.ContainsKey(id))
            {
                throw new ArgumentOutOfRangeException();
            }
            return cellsById[id];
        }

        public void MoveFigureFromTo(int id, Vector2Int from, Vector2Int to)
        {
            if (!IsWithinBoard(from.x, from.y) || !IsWithinBoard(to.x, to.y))
                throw new ArgumentOutOfRangeException();

            if (board[from.x, from.y].CurrentFigure?.Id != id)
                throw new Exception($"No such figure (id={id}) at cell [{from.x},{from.y}]");

            if (board[to.x, to.y].CurrentFigure != null)
                throw new Exception($"Can't move figure at occupied cell [{to.x}, {to.y}]");

            board[to.x, to.y].CurrentFigure = board[from.x, from.y].CurrentFigure;
            cellsByFigureId[id] = board[to.x, to.y];
            board[from.x, from.y].CurrentFigure = null;
        }

        public FigureColor CheckSomebodyWonGame()
        {
            if (StartCells[FigureColor.Light].All(x => x.CurrentFigure?.Color == FigureColor.Dark))
                return FigureColor.Dark;
            else if (StartCells[FigureColor.Dark].All(x => x.CurrentFigure?.Color == FigureColor.Light))
                return FigureColor.Light;
            else
                return FigureColor.None;
        }

        public void ResetGame()
        {
            foreach (var c in board)
            {
                c.CurrentFigure = null;
            }
            ResetFigurePositions(FigureColor.Light);
            ResetFigurePositions(FigureColor.Dark);
        }

        private void ResetFigurePositions(FigureColor side)
        {
            int index = 0;
            foreach (var f in figures.Values.Where(x => x.Color == side))
            {
                StartCells[side][index++].CurrentFigure = f;
            }
        }

        public bool IsWithinBoard(int x, int y)
        {
            return x >= 0 && x < Size && y >= 0 && y < Size;
        }
    }
}
