﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ugolki
{
    /// <summary>
    /// Данные клетки игрового поля.
    /// </summary>
    public class Cell
    {
        public readonly int Id;
        public readonly Vector2Int Position;
        public readonly CellColor Color;
        public Figure CurrentFigure;

        public Cell(int id, int x, int y, CellColor color)
        {
            if (id < 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(id)}");
            }
            if (x < 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(x)}");
            }
            if (y < 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(y)}");
            }

            Id = id;
            Position = new Vector2Int(x, y);
            Color = color;
            CurrentFigure = null;
        }

    }
}
