﻿namespace Ugolki
{
    public class Figure
    {
        public readonly int Id;
        public readonly FigureColor Color;

        public Figure(int id, FigureColor color)
        {
            Id = id;
            Color = color;
        }
    }
}
