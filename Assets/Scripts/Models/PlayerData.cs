﻿namespace Ugolki
{
    /// <summary>
    /// Данные игрока.
    /// </summary>
    public class PlayerData
    {
        public readonly int Id;
        public readonly string Name;
        public readonly FigureColor Side;
        public readonly UnityEngine.Color Color;
        public int MoveNumber;

        public PlayerData(int id, string name, FigureColor side, UnityEngine.Color color)
        {
            Id = id;
            Name = name;
            Side = side;
            Color = color;
            MoveNumber = 0;
        }
    }
}
