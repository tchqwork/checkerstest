﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Ugolki
{
	[System.Serializable]
	public class GameModeEvent : UnityEvent<GameMode>
	{

	}
	/// <summary>
	/// Класс-помощник, позволяющий задавать в редакторе значения, передаваемые по клику кнопки в нужные функции, сразу через enum, вместо конверсии через int и т.п.
	/// </summary>
	public class GameModeButtonEvent : MonoBehaviour
    {
		public GameMode Mode;
		public GameModeEvent Event;

		public void Invoke()
		{
			Event?.Invoke(Mode);
		}
	}
}
