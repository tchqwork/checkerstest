﻿using UnityEngine;

namespace Ugolki
{
    /// <summary>
    /// Реализация синглтона. Рассматривал только ситуацию, когда нужные объекты уже должны быть в сцене.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();
                }
                return _instance;
            }
        }

        protected virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = this as T;
            }
            else
            {
                Debug.LogError($"There should be only one {gameObject}");
                Destroy(gameObject);
            }
        }
    }
}
