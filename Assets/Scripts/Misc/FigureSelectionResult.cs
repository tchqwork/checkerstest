﻿using System.Collections.Generic;
using UnityEngine;

namespace Ugolki
{
    /// <summary>
    /// Класс для кеширования результатов выбора фигур на поле
    /// </summary>
    public class FigureSelectionResult
    {
        public readonly int FigureId;
        public readonly List<Cell> PossibleMoves;
        public readonly Vector2Int Position;

        public FigureSelectionResult(int figureId, Vector2Int pos, List<Cell> cells)
        {
            FigureId = figureId;
            Position = pos;
            PossibleMoves = cells;
        }
    }
}
