﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Ugolki
{
    /// <summary>
    /// По заданным координитам фигуры получаем список ячеек, куда ее можно переместить.
    /// </summary>
    public interface IFigureMover
    {
        List<Cell> GetPossibleMoves(Gameboard board, int x, int y);
    }
}
