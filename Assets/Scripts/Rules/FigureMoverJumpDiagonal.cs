﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Ugolki
{
    /// <summary>
    /// Перемещение фигуры только в пределах соседних незанятых клеток и перепрыгивать по диагонали через фигуры противника.
    /// </summary>
    public class FigureMoverJumpDiagonal : IFigureMover
    {
        public static readonly FigureMoverJumpDiagonal Default = new FigureMoverJumpDiagonal();

        protected FigureMoverJumpDiagonal()
        {
        }

        public List<Cell> GetPossibleMoves(Gameboard board, int x, int y)
        {
            var result = FigureMoverJustMove.Default.GetPossibleMoves(board, x, y);

            // Клетки, где могут быть вражеские фигуры.
            var checkFigurePoints = new List<Vector2Int>() { new Vector2Int(x - 1, y - 1), new Vector2Int(x - 1, y + 1), new Vector2Int(x + 1, y - 1), new Vector2Int(x + 1, y + 1) };
            // Клетки, куда мы теоретически можем прыгнуть.
            var jumpToPoints = new List<Vector2Int>() { new Vector2Int(x - 2, y - 2), new Vector2Int(x - 2, y + 2), new Vector2Int(x + 2, y - 2), new Vector2Int(x + 2, y + 2) };

            for (int i = 0; i < checkFigurePoints.Count; i++)
            {
                if (board.IsWithinBoard(jumpToPoints[i].x, jumpToPoints[i].y)
                    && board[jumpToPoints[i].x, jumpToPoints[i].y].CurrentFigure == null
                    && board[checkFigurePoints[i].x, checkFigurePoints[i].y].CurrentFigure != null
                    && board[checkFigurePoints[i].x, checkFigurePoints[i].y].CurrentFigure.Color != board[x, y].CurrentFigure.Color)
                {
                    result.Add(board[jumpToPoints[i].x, jumpToPoints[i].y]);
                }
            }

            return result;
        }
    }
}
