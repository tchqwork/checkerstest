﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Ugolki
{
    /// <summary>
    /// Перемещение фигуры только в пределах соседних незанятых клеток
    /// </summary>
    public class FigureMoverJustMove : IFigureMover
    {
        public static readonly FigureMoverJustMove Default = new FigureMoverJustMove();

        protected FigureMoverJustMove()
        {
        }

        public List<Cell> GetPossibleMoves(Gameboard board, int x, int y)
        {
            if (board == null)
                throw new ArgumentNullException(nameof(board));

            if (x < 0 || x > board.Size)
                throw new ArgumentOutOfRangeException("");

            if (y < 0 || y > board.Size)
                throw new ArgumentOutOfRangeException("");

            if (board[x, y].CurrentFigure == null)
                throw new Exception($"No figure at {x}:{y}");

            var result = new List<Cell>();

            int xMin = x - 1 < 0 ? x : x - 1;
            int xMax = x + 1 > board.Size - 1 ? x : x + 1;
            int yMin = y - 1 < 0 ? y : y - 1;
            int yMax = y + 1 > board.Size - 1 ? y : y + 1;

            Debug.Log($"GetPossibleMoves at {x}:{y}, x=[{xMin},{xMax}], y=[{yMin},{yMax}]");

            for (int i = xMin; i <= xMax; i++)
            {
                for (int j = yMin; j <= yMax; j++)
                {
                    if ((i != x || j != y) && board[i, j].CurrentFigure == null)
                    {
                        result.Add(board[i, j]);
                    }
                }
            }

            return result;
        }
    }
}
