﻿using System.Collections.Generic;
using UnityEngine;

namespace Ugolki
{
    /// <summary>
    /// Перемещение фигуры только в пределах соседних незанятых клеток и перепрыгивать по вертикали и горизонтали через фигуры противника.
    /// </summary>
    public class FigureMoverJumpHorizontalVertical : IFigureMover
    {
        public static readonly FigureMoverJumpHorizontalVertical Default = new FigureMoverJumpHorizontalVertical();

        protected FigureMoverJumpHorizontalVertical()
        {
        }

        public List<Cell> GetPossibleMoves(Gameboard board, int x, int y)
        {
            var result = FigureMoverJustMove.Default.GetPossibleMoves(board, x, y);

            // Клетки, где могут быть вражеские фигуры.
            var checkFigurePoints = new List<Vector2Int>() { new Vector2Int(x - 1, y), new Vector2Int(x + 1, y), new Vector2Int(x, y - 1), new Vector2Int(x, y + 1) };
            // Клетки, куда мы теоретически можем прыгнуть.
            var jumpToPoints = new List<Vector2Int>() { new Vector2Int(x - 2, y), new Vector2Int(x + 2, y), new Vector2Int(x, y - 2), new Vector2Int(x, y + 2) };

            for (int i = 0; i < checkFigurePoints.Count; i++)
            {
                if (board.IsWithinBoard(jumpToPoints[i].x, jumpToPoints[i].y)
                    && board[jumpToPoints[i].x, jumpToPoints[i].y].CurrentFigure == null
                    && board[checkFigurePoints[i].x, checkFigurePoints[i].y].CurrentFigure != null
                    && board[checkFigurePoints[i].x, checkFigurePoints[i].y].CurrentFigure.Color != board[x, y].CurrentFigure.Color)
                {
                    result.Add(board[jumpToPoints[i].x, jumpToPoints[i].y]);
                }
            }

            return result;
        }
    }
}
