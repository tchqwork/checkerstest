﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ugolki
{
    public class PlayerView : MonoBehaviour
    {
        [SerializeField] Image PlayerImage;
        [SerializeField] Text PlayerName;
        [SerializeField] Text PlayerMoveNumber;

        public void Init(string name, Color color)
        {
            PlayerImage.color = color;
            PlayerName.text = name;
            UpdateMoveNumber(0);
        }

        public void UpdateMoveNumber(int n)
        {
            PlayerMoveNumber.text = $"Ход: {n}";
        }
    }
}