﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Ugolki
{
    /// <summary>
    /// Представление фигуры на сцене
    /// </summary>
    public class FigureView : MonoBehaviour, IPointerClickHandler
    {
        public int Id { get; private set; }
        [SerializeField] new Renderer renderer;

        public void Init(int id, Material material)
        {
            Id = id;
            renderer.material = material;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log($"Figure clicked, id = {Id}");
            GameController.Instance.FigureClicked(Id);
        }
    }
}
