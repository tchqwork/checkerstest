﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Ugolki
{
    /// <summary>
    /// Представление ячейки на сцене
    /// </summary>
    public class CellView : MonoBehaviour, IPointerClickHandler
    {
        public int Id { get; private set; }
        /// <summary>
        /// Маркер, показывающий, что выбранную фигуру можно передвинуть на это поле
        /// </summary>
        [SerializeField] GameObject CanMoveToCellMarker;
        [SerializeField] Renderer CellViewRenderer;

        public void Init(Cell cell, Material material)
        {
            Id = cell.Id;
            CellViewRenderer.material = material;
        }

        public void MoveMarkerOn()
        {
            //Debug.Log($"CellView {Id} enable move marker");
            CanMoveToCellMarker?.SetActive(true);
        }

        public void MoveMarkerOff()
        {
            CanMoveToCellMarker?.SetActive(false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log($"Cell clicked, id = {Id}");
            GameController.Instance.CellClicked(Id);
        }
    }
}