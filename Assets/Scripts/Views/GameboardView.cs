﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ugolki
{
    public class GameboardView : MonoBehaviour
    {
        // Материалы для двух типов фигур и двух типов клоток - условные белые и чёрные
        [SerializeField] Material FirstCellMaterial;
        [SerializeField] Material SecondCellMaterial;
        [SerializeField] Material FirstFigureMaterial;
        [SerializeField] Material SecondFigureMaterial;

        [SerializeField] Transform CellsContainer;
        [SerializeField] Transform FiguresContainer;

        readonly string cellPrefabName = "Prefabs\\CellPrefab";
        readonly string figurePrefabName = "Prefabs\\FigurePrefab";

        // Словари для быстрого доступа к данным по id
        public Dictionary<int, CellView> Cells = new Dictionary<int, CellView>();
        public Dictionary<int, FigureView> Figures = new Dictionary<int, FigureView>();

        protected Tweener _tweener;

        public void Init(Gameboard board)
        {
            var cellPrefab = Resources.Load<CellView>(cellPrefabName);
            var cellBounds = cellPrefab.GetComponent<BoxCollider>().size;
            var figurePrefab = Resources.Load<FigureView>(figurePrefabName);

            for (int i = 0; i < board.Size; i++)
            {
                for (int j = 0; j < board.Size; j++)
                {
                    var cellView = Instantiate(cellPrefab,
                                        new Vector3((i - board.Size / 2f) * cellBounds.x, 0f, (j - board.Size / 2f) * cellBounds.z),
                                        Quaternion.identity,
                                        CellsContainer);
                    cellView.name = $"Cell [{i}:{j}]";
                    cellView.Init(board[i, j], board[i,j].Color == CellColor.Light ? FirstCellMaterial : SecondCellMaterial);
                    Cells[board[i,j].Id] = cellView;

                    if (board[i,j].CurrentFigure != null)
                    {
                        var figureView = Instantiate(figurePrefab, cellView.transform.position, Quaternion.identity, FiguresContainer);
                        figureView.name = $"Figure Id_{board[i, j].CurrentFigure.Id}";
                        figureView.Init(board[i, j].CurrentFigure.Id,
                                        board[i, j].CurrentFigure.Color == FigureColor.Light ? FirstFigureMaterial : SecondFigureMaterial);
                        Figures[board[i, j].CurrentFigure.Id] = figureView;
                    }
                }
            }
        }

        public void ResetGame(Gameboard board)
        {
            DOTween.KillAll();

            for (int i = 0; i < board.Size; i++)
            {
                for (int j = 0; j < board.Size; j++)
                {
                    if (board[i, j].CurrentFigure != null)
                    {
                        Figures[board[i, j].CurrentFigure.Id].transform.position = Cells[board[i, j].Id].transform.position;
                    }
                }
            }
        }

        public void TurnHighlightOff()
        {
            foreach (var c in Cells.Values)
            {
                c.MoveMarkerOff();
            }
        }
        /// <summary>
        /// Включить маркеры на всех указанных ячейках
        /// Они должны подсветить ячейки, куда можно переместить фигуру
        /// </summary>
        /// <param name="cells"></param>
        public void HighlightCells(List<Cell> cells)
        {
            Debug.Log($"Cells count = {cells?.Count}");
            TurnHighlightOff();

            if (cells != null)
            {
                //cells?.ForEach(x => Debug.Log($"highlight cell id = {x.Id}"));

                foreach (var c in cells)
                {
                    Cells[c.Id].MoveMarkerOn();
                }
            }
        }

        public void MoveFigureToCell(int figureId, int cellId, TweenCallback OnEndMove)
        {
            Figures[figureId].transform.DOMove(Cells[cellId].transform.position, 0.3f).OnComplete(OnEndMove);
        }

        /// <summary>
        /// Получаем текущий цвет нужного типа фигуры.
        /// Сделано просто, чтобы можно было отображать тот же цвет в UI для игроков.
        /// </summary>
        /// <param name="side"></param>
        /// <returns></returns>
        public Color GetColor(FigureColor side)
        {
            switch(side)
            {
                case FigureColor.Light:
                    return FirstCellMaterial.color;
                case FigureColor.Dark:
                    return SecondFigureMaterial.color;
                default:
                    Debug.LogError($"Trying to get color for unknown side = {side}");
                    return Color.magenta;
            }
        }
    }
}