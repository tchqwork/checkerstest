﻿namespace Ugolki
{
    /// <summary>
    /// Цвет (тип) фигуры.
    /// </summary>
    public enum FigureColor
    {
        None,
        Light,
        Dark
    }
}
