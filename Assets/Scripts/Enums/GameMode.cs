﻿using System;

namespace Ugolki
{
    /// <summary>
    /// Тип игры, определяющий движения фигур.
    /// </summary>
    [Serializable]
    public enum GameMode
    {
        JustMove,
        JumpDiagonal,
        JumpVerticalHorizontal
    }
}
