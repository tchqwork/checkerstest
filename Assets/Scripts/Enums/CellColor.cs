﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ugolki
{
    /// <summary>
    /// Цвет (тип) клетки игрового поля.
    /// </summary>
    public enum CellColor
    {
        Light,
        Dark
    }
}
