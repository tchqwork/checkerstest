﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

namespace Ugolki
{
    public class UIController : SingletonBehaviour<UIController>
    {
        [SerializeField] List<PlayerView> PlayerViewItems;
        [SerializeField] GameObject GameModeSelectionPanel;
        [SerializeField] GameObject WinPanel;
        [SerializeField] GameObject LoadingPanel;
        [SerializeField] Text PlayerWonName;
        [SerializeField] Text CurrentMovePlayerName;

        public void LoadingDone()
        {
            LoadingPanel.SetActive(false);
        }

        public void ShowGameModeSelectionPanel()
        {
            WinPanel.SetActive(false);
            GameModeSelectionPanel.SetActive(true);
        }

        public void SetPlayers(PlayerData[] players)
        {
            if (players.Length != PlayerViewItems.Count)
            {
                throw new Exception("Players and player's views count do not match");
            }

            for (int i = 0; i < PlayerViewItems.Count; i++)
            {
                PlayerViewItems[i].Init(players[i].Name, players[i].Color);
            }
        }
        /// <summary>
        /// Обновляем информацию по сделанным ходам.
        /// </summary>
        /// <param name="players"></param>
        public void UpdateMoves(PlayerData[] players)
        {
            for (int i = 0; i < PlayerViewItems.Count; i++)
            {
                PlayerViewItems[i].UpdateMoveNumber(players[i].MoveNumber);
            }
        }
        /// <summary>
        /// Указываем игрока, который сейчас должен ходить.
        /// </summary>
        /// <param name="name"></param>
        public void UpdateCurrentMovePlayerName(string name)
        {
            CurrentMovePlayerName.text = name;
        }

        public void ShowWinPanel(string playerWon)
        {
            PlayerWonName.text = playerWon;
            WinPanel.SetActive(true);
        }
    }
}
