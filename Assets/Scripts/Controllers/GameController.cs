﻿using UnityEngine;
using UnityEngine.Events;

namespace Ugolki
{
    public class GameController : SingletonBehaviour<GameController>
    {
        /// <summary>
        /// Размер поля. Считаем, что поле квадратное.
        /// </summary>
        [SerializeField] int boardSize = 8;
        /// <summary>
        /// Размер стартовой локации каждого из игроков. Также квадратной.
        /// </summary>
        [SerializeField] int startFieldSize = 3;
        [SerializeField] Transform GameboardViewContainer;
        [SerializeField] public UnityEvent OnInitCompleted;
        public Gameboard Board { get; private set; }
        public GameboardView BoardView { get; private set; }
        public PlayerData[] Players { get; private set; }

        /// <summary>
        /// Признак необходимости блокировать пользоватльский ввод.
        /// Скажем, ещё не закончилась анимация у противника.
        /// </summary>
        public bool BlockInput { get; private set; }
        /// <summary>
        /// Номер текущего хода.
        /// Увеличивается после хода каждого из игроков.
        /// </summary>
        int overallMoveNumber = 0;
        /// <summary>
        /// Выбранное правило перемещения фигур.
        /// </summary>
        IFigureMover Mover = FigureMoverJustMove.Default;
        /// <summary>
        /// Кешированные данные по выбранной фигуре - клетки, куда можно ходить и т.п.
        /// </summary>
        FigureSelectionResult selectedFigure = null;

        string gameboardViewPrefabName = "Prefabs\\GameboardViewPrefab";

        protected override void Awake()
        {
            base.Awake();
            Init();
        }

        public void Init()
        {
            BlockInput = true;
            overallMoveNumber = 0;

            Board = new Gameboard(boardSize, startFieldSize);

            var gameboardViewPrefab = Resources.Load<GameboardView>(gameboardViewPrefabName);
            BoardView = Instantiate(gameboardViewPrefab, GameboardViewContainer);
            BoardView.Init(Board);

            Players = new PlayerData[] {
                new PlayerData(0, "Вася", FigureColor.Light, BoardView.GetColor(FigureColor.Light)),
                new PlayerData(1, "Коля", FigureColor.Dark, BoardView.GetColor(FigureColor.Dark))
            };

            UIController.Instance.SetPlayers(Players);
            UIController.Instance.UpdateCurrentMovePlayerName(Players[overallMoveNumber % Players.Length].Name);
            BlockInput = false;
            OnInitCompleted?.Invoke();
        }

        public void SelectGameMode(GameMode mode)
        {
            Debug.Log($"Select game mode = '{mode}'");
            switch(mode)
            {
                case GameMode.JustMove:
                    Mover = FigureMoverJustMove.Default;
                    break;
                case GameMode.JumpDiagonal:
                    Mover = FigureMoverJumpDiagonal.Default;
                    break;
                case GameMode.JumpVerticalHorizontal:
                    Mover = FigureMoverJumpHorizontalVertical.Default;
                    break;
                default:
                    Mover = FigureMoverJustMove.Default;
                    Debug.LogError($"Unknown selected game mode '{mode}'");
                    break;
            }
        }

        public void RestartGame()
        {
            overallMoveNumber = 0;
            BlockInput = false;
            selectedFigure = null;

            Board.ResetGame();
            BoardView.ResetGame(Board);
            BoardView.TurnHighlightOff();

            Players[0].MoveNumber = 0;
            Players[1].MoveNumber = 0;
            UIController.Instance.UpdateMoves(Players);
            UIController.Instance.UpdateCurrentMovePlayerName(Players[overallMoveNumber % Players.Length].Name);
            UIController.Instance.ShowGameModeSelectionPanel();
        }

        public void FigureClicked(int figureId)
        {
            if (BlockInput)
                return;

            var figure = Board.GetFigure(figureId);
            if (Players[overallMoveNumber % Players.Length].Side != figure?.Color)
                return;

            if (selectedFigure?.FigureId == figureId)
                return;

            var cell = Board.GetCellByFigureId(figureId);
            if (cell != null)
            {
                var cells = Mover.GetPossibleMoves(Board, cell.Position.x, cell.Position.y);
                selectedFigure = new FigureSelectionResult(figureId, cell.Position, cells);
                BoardView.HighlightCells(cells);
            }
            else
            {
                selectedFigure = null;
                Debug.LogError($"Can't find cell with Figure id = {figureId}");
            }
        }

        public void CellClicked(int cellId)
        {
            if (selectedFigure == null || BlockInput)
                return;

            var cell = Board.GetCellById(cellId);
            if (cell != null)
            {
                var cellToMove = selectedFigure.PossibleMoves.Find(x => x.Id == cell.Id);

                if (cellToMove != null)
                {
                    BlockInput = true;
                    Board.MoveFigureFromTo(selectedFigure.FigureId, selectedFigure.Position, cellToMove.Position);
                    BoardView.TurnHighlightOff();
                    BoardView.MoveFigureToCell(selectedFigure.FigureId, cellToMove.Id, NextMove);
                }
            }
            else
            {
                Debug.LogError($"Can't find cell with id = {cellId}");
            }
        }

        public void NextMove()
        {
            BlockInput = false;
            var wonSide = Board.CheckSomebodyWonGame();
            if (wonSide == FigureColor.None)
            {
                selectedFigure = null;
                Players[overallMoveNumber % Players.Length].MoveNumber++;
                BoardView.TurnHighlightOff();
                UIController.Instance.UpdateMoves(Players);
                overallMoveNumber++;
                UIController.Instance.UpdateCurrentMovePlayerName(Players[overallMoveNumber % Players.Length].Name);
            }
            else
            {
                UIController.Instance.ShowWinPanel(Players[0].Side == wonSide ? Players[0].Name : Players[1].Name);
            }
        }
    }
}